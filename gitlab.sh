#!/bin/bash
# Install build dependencies for gitlab ci

apt-get update -y
apt-get install -y --fix-missing wget xvfb chrpath libfuse-dev
